'use strict'

const Game = require('../../sources/engine/Game');
const Player = require('../../sources/engine/Player');
const JokeCard = require('../../sources/engine/JokeCard');
const jsonJokes = require("../../public/jsons/jokeCard.json");
const jsonQuotes = require("../../public/jsons/quotes.json");

describe('Game test', () =>
{
	test('Game should be initialized with no players / an certain amout of cards / leaderboard', () => 
	{
		let game = new Game();

		expect(Object.values(game.players).length).toBe(0);
		expect(Object.values(game.leaderboard).length).toBe(0);

		// Give quote inside the game constructor : 53-1 --> 52
		expect(Object.values(game.stockCardQuotes).length).toBe(52);
		expect(Object.values(game.stockCardJokes).length).toBe(53);
	});

	test('Registration and delistation', () => 
	{
		let game = new Game();

		game.register('joueur1');
		game.register('joueur2');
		expect(Object.values(game.players).length).toBe(2);

		game.delist('joueur1');
		game.delist('joueur2');
		expect(Object.values(game.players).length).toBe(0);
	});

	test('Filling the deck of cards', () => 
	{
		let game = new Game();

		// Open JsonQuotes
		jsonQuotes.forEach(function(element, i)
		{
			// Verify the given quote in the constructor
			if(game.stockCardQuotes[i] == undefined)
			{
				expect(game.currentQuote.id).toBe(i);
			}
			else
			{
				expect(game.stockCardQuotes[i].id).toBe(element.id);
				expect(game.stockCardQuotes[i].joke).toBe(element.joke);
			}
		});

		// Open JsonJokes
		jsonJokes.forEach(function(element, i)
		{
			expect(game.stockCardJokes[i].id).toBe(element.id);
			expect(game.stockCardJokes[i].joke).toBe(element.joke);
		});
	});

	test('Giving quotes', () => 
	{
		let game = new Game();
		
		// All card should be there, except the first quote who was drawn
		for(let i = 0; i < 52; i++)
			expect(game.giveQuote()).toBeDefined();

		//When there is no more card, we pick a undifined and not a card
		expect(game.giveQuote()).toBeUndefined();
	});

	test('keepWinners / Update Score case : single winner', () => 
	{
		let game = new Game();

		let player1 = new Player("player1", game);
		let player2 = new Player("player2", game);
		let player3 = new Player("player3", game);
		let player4 = new Player("player4", game);

		game.players = {player1, player2, player3, player4};

		let carte1 = new JokeCard("jc1","Une première blague");
		let carte2 = new JokeCard("jc2","Une deuxième blague");
		let carte3 = new JokeCard("jc3","Une troisième blague");
		let carte4 = new JokeCard("jc4","Une quatrième blague");

		carte1.score = 2;	carte1.player = "player1";
		carte2.score = 3;	carte2.player = "player2";
		carte3.score = 1;	carte3.player = "player3";
		carte4.score = 1;	carte4.player = "player4";

		let nonSortedObjectArray = {carte1, carte2, carte3, carte4};
		let winners = game.keepWinners(nonSortedObjectArray);

		expect(winners).toStrictEqual([carte2]);
		
		// --------------------------------------------

		// Expected output
		let temp = [player1, player2, player3, player4];

		Object.values(game.leaderboard).forEach(function(element, i)
		{
			expect(element).toBe(temp[i]);
			expect(element.score).toBe(0);
		});
		
		// Updating and sorting leaderboard scores
		game.updateScore(winners);
		game.leaderboard = game.getSortedLeaderboard();

		// Expected output
		temp = [player2, player1, player3, player4];

		// Compare each elements one by one : in fact this test :
		// expect(game.leaderboard).toEqual({player2, player1, player3, player4});
		// Wasn't efective, it didn't verify if leaderboard has the right order of objects
		Object.values(game.leaderboard).forEach( function(element, i)
		{
			expect(element).toBe(temp[i]);

			if(element.id == "player2")
			{
				expect(element.score).toBe(1);
			}
			else
			{
				expect(element.score).toBe(0);
			}
		});
	});

	test('keepWinners / Update Score case : multiple winners', () => 
	{
		let game = new Game();

		let player1 = new Player("player1", game);
		let player2 = new Player("player2", game);
		let player3 = new Player("player3", game);
		let player4 = new Player("player4", game);

		game.players = {player1, player2, player3, player4};

		let carte1 = new JokeCard("jc1","Une première blague");
		let carte2 = new JokeCard("jc2","Une deuxième blague");
		let carte3 = new JokeCard("jc3","Une troisième blague");
		let carte4 = new JokeCard("jc4","Une quatrième blague");

		carte1.score = 0;	carte1.player = "player1";
		carte2.score = 0;	carte2.player = "player2";
		carte3.score = 2;	carte3.player = "player3";
		carte4.score = 2;	carte4.player = "player4";

		let nonSortedObjectArray = {carte1, carte2, carte3, carte4};
		let winners = game.keepWinners(nonSortedObjectArray);

		expect(winners).toStrictEqual([carte3, carte4]);
		
		// --------------------------------------------

		// Expected output
		let temp = [player1, player2, player3, player4];

		Object.values(game.leaderboard).forEach(function(element, i)
		{
			expect(element).toBe(temp[i]);
			expect(element.score).toBe(0);
		});
		
		// Updating and sorting leaderboard scores
		game.updateScore(winners);
		game.leaderboard = game.getSortedLeaderboard();

		// Expected output
		temp = [player3, player4, player1, player2];

		// Compare each elements one by one : in fact this test :
		// expect(game.leaderboard).toEqual({player2, player1, player3, player4});
		// Wasn't efective, it didn't verify if leaderboard has the right order of objects
		Object.values(game.leaderboard).forEach(function(element, i)
		{
			expect(element).toBe(temp[i]);

			if(element.id == "player3" || element.id == "player4")
			{
				expect(element.score).toBe(1);
			}
			else
			{
				expect(element.score).toBe(0);
			}
		});
	});

	test('Isfinnished case : no more quotes', () => 
	{
		let game = new Game();

		expect(game.isFinished()).toBe(false);

		// Copy of length because the stockCardQuotes.length will decrease
		let copieLength = Object.values(game.stockCardQuotes).length;

		// Draw all the quotes
		for(let i = 0; i < copieLength; i++)
		{
			game.currentQuote = game.giveQuote();
		}
		expect(game.isFinished()).toBe(true);
	});

	test('Isfinnished case : no more Jokes', () => 
	{
		let game = new Game();

		expect(game.isFinished()).toBe(false);

		let player1 = new Player("player1", game);

		// Copy of length because the stockCardJokes.length will decrease
		let copieLength = Object.values(game.stockCardJokes).length;

		// Draw all the jokes. Because of our limit we play one card for being able drawing a card we play one to delete it.
		for(let i = 0; i < copieLength; i++)
		{
			player1.drawCard(game);
			player1.playCard(Object.values(player1.stockCardPlayer)[0].id)
		}
		expect(game.isFinished()).toBe(true);
	});

	test('Isfinnished case : a player got the required score', () => 
	{
		let game = new Game();

		expect(game.isFinished()).toBe(false);

		let player1 = new Player("player1", game);
		game.players = {player1};

		player1.score = 3;

		expect(game.isFinished()).toBe(true);
	});

	test('Giving back cards', () => 
	{
		let game = new Game();

		expect(Object.values(game.stockCardJokes).length).toBe(53);

		let player1 = new Player("player1", game);
		game.players = {player1};

		expect(Object.values(game.stockCardJokes).length).toBe(49);

		game.giveBackCards("player1");

		expect(Object.values(game.stockCardJokes).length).toBe(53);
	});
});