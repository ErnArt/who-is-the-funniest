'use strict'

const Player = require('../../sources/engine/Player');
const Game = require('../../sources/engine/Game');

describe('Player test', () => 
{	
	test('Creation of a Player', () => 
	{
		const game = new Game();
		const player = new Player("joueur1", game);

		expect(player.id).toBe("joueur1");
		expect(player.score).toBe(0);
		expect(Object.values(player.stockCardPlayer).length).toBe(4);
	});

	test('Player should draw / play a card', () => 
	{	
		const game = new Game();
		const player = new Player("joueur1", game);
	
		expect(Object.values(player.stockCardPlayer).length).toBe(4);
		expect(Object.values(game.stockCardJokes).length).toBe(49);

		player.playCard(Object.values(player.stockCardPlayer)[0].id)

		expect(Object.values(player.stockCardPlayer).length).toBe(3);
		expect(Object.values(game.stockCardJokes).length).toBe(49);

		player.drawCard(game);

		expect(Object.values(player.stockCardPlayer).length).toBe(4);
		expect(Object.values(game.stockCardJokes).length).toBe(48);
	});
});