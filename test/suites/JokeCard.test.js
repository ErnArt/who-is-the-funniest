'use strict'

const JokeCard = require('../../sources/engine/JokeCard');

describe('JokeCard test', () => 
{
	test('Creation of a Jokecard', () => 
	{
		let carte1 = new JokeCard("jc1","Une blague");

		expect(carte1.id).toBe("jc1");
		expect(carte1.joke).toBe("Une blague");
		expect(carte1.player).toBe(0);
		expect(carte1.score).toBe(0)
	});
});