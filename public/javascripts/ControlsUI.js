/**
 * Representation of our Control ui class. This class prints player cards and voted card in the ui.
 * @class
 */
class ControlsUI
{
	/**
	 * Create a control ui instance with a socket to keep listening to emissions and the mainBoard as a div access point.
	 * @param {ControlsSocket}	socket		A ControlsSocket to keep listening and emitting messages.
	 */
	constructor(socket)
	{
		this.socket = socket;
		this.mainBoard = document.querySelector("#mainBoard");

		// Creation of two listeners for printing the client cards and voted cards.
		this.listenerPrintCardsClient();
		this.listenerPrintCardsPlayed();

		// Waiting for the signal tooManyPlayers to print in the player page that he has to wait.
		// We verify with playerID that is the socket.id given by the server if it's the current player, else it would
		// print it to all players.
		this.socket.stockSocket.on("tooManyPlayers", (playerID) =>
		{
			if (playerID == this.socket.stockSocket.id)
				document.querySelector("#playerID").innerHTML = "Too many players or not enough cards. Please wait.";
		});
	}

	/**
	 * Create a listener with our stockSocket attribute of socket for the message printCardsClient.
	 */
	listenerPrintCardsClient()
	{
		this.socket.stockSocket.on('printCardsClient', (playerID, cardClientFromServer) => 
		{
			// If we disabled the mainBoard when voting a card we enable it.
			if (this.mainBoard.style.display = "none")
				this.mainBoard.style.display = null;

			// This verification helps to only print the current player cards because the server emits all the cards for all the players.
			if (playerID == this.socket.stockSocket.id)
			{
				// We store in a local argument the cards from the server and print them.
				this.cardClient = cardClientFromServer;
				this.printCardsArray(this.cardClient);

				// We add a listeners of the onclick event for each cards.
				this.cardArray = document.querySelectorAll(".cardPlayer");
				this.cardArray.forEach(element =>
				{
					element.onclick = (event) =>
					{
						// If the card exists in our array we play this card.
						let card = this.searchCardArray(element, this.cardClient);
						if (card != false)
							this.playCard(event, card);
					}
				});
			}
		});
	}

	/**
	 * Create a listener with our stockSocket attribute of socket for the message printCardsPlayed.
	 */
	listenerPrintCardsPlayed()
	{
		this.socket.stockSocket.on('printCardsPlayed', (cardPlayedFromServer) => 
		{
			// We store in a local argument the cards from the server and print them.
			this.cardPlayed = cardPlayedFromServer;
			this.printCardsArray(this.cardPlayed);

			// We add a listeners of the onclick event for each cards.
			this.cardArray = document.querySelectorAll(".cardPlayer");
			this.cardArray.forEach(element => 
			{
				element.onclick = (event) => 
				{
					// If the card exists in our array we play this card.
					let card = this.searchCardArray(element, cardPlayedFromServer);
					if (card != false)
					{
						// We disable the mainBoard to avoid voting twice (in the server we forbid a player to vote twice too).
						this.chosePlayedCard(card);
						this.mainBoard.style.display = "none";
					}
				}
			});
		});
	}

	/**
	 * Search a card in a whole array.
	 * @param {JokeCard}	card		A card.
	 * @param {array}		cardArray	An array with all the players card.
	 */
	searchCardArray(card, cardArray)
	{
		// We cannot use a forEach loop because there is no way to break or return in these loops. We then use an iterator.
		// If we find the card in the array we return the correct card.
		for (let element of cardArray)
			if (element.id == parseInt(card.childNodes[1].textContent))
				return element;

		// We should never return false because we only print card of the player.
		return false;
	}

	/**
	 * Print the jokes on the main board.
	 * @param {array}		jokes		A joke array.
	 */
	printCardsArray(jokes)
	{
		this.mainBoard.innerHTML = "";

		// We go through the whole array and for each element we create a card.
		jokes.forEach( element =>
		{
			let html = 
			`<div class="card cardPlayer" style="width: 18rem;">
				<div class="card-header">` + element.id + `</div>
				<div class="card-body"><p class="card-text">` + element.joke + `</p></div>
				<a href="#" class="stretched-link"></a>
			</div>`
			this.mainBoard.innerHTML += html;
		});
	}

	/**
	 * Launch the playCard function.
	 * @param {event}		event	An event.
	 * @param {JokeCard}	card	A joke card.
	 */
	playCard(event, card)
	{
		this.socket.playCard(card);
	}

	/**
	 * Launch the chosePlayedCard function.
	 * @param {JokeCard}	card	A joke card.
	 */
	chosePlayedCard(card)
	{
		this.socket.chosePlayedCard(card);
	}
}
