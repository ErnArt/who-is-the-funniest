/**
 * Representation of our Game ui class. This class prints jokes quotes and the leaderboard.
 */
class GameUI
{
	/**
	 * Create a game ui instance with ui and quote as two div access points.
	 */
	constructor()
	{
		this.cards = document.querySelector("#mainBoardCards");
		this.leader = document.querySelector("#mainBoardLeader");
		this.quote = document.querySelector("#quote");
		this.emptyBackground();
	}

	/**
	 * Empty the quote and the card board.
	 */
	emptyBackground()
	{
		this.quote.innerHTML = "";
		this.leader.innerHTML = "";
		this.cards.innerHTML = "";
	}

	/**
	 * Print the quote on the main board.
	 * @param {string}	quote			A quote given by the server from the main game class.
	 */
	printQuote(quote)
	{
		this.emptyBackground();
		this.quote.innerHTML = quote;
	}

	/**
	 * Print the leaderboard on the main board. If the game is finished we change the title.
	 * @param {array}	leaderboard		A sorted array given by the server representing the leaderboard.
	 * @param {array}	winningCards	The winning cards from the turn.
	 * @param {boolan}	isFinished		The boolean given by the server to know if the game is finished.
	 */
	printLeaderboard(leaderboard, winningCards, isFinished)
	{
		let textLeaderboard = "Leaderboard"

		if (isFinished)
			textLeaderboard = "End of game ! The winner is..."
			
		this.printJokes(winningCards, textLeaderboard);

		// We go through the whole array and for each element we create a card.
		leaderboard.forEach( element => 
		{
			let html = 
			`<div class="card cardPlayer" style="width: 18rem;">
				<div class="card-header">` + element.id + `</div>
				<div class="card-body"><p class="card-text">` + element.score + `</p></div>
			</div>`
			this.leader.innerHTML += html;
		});

		// When we printed we return true
		return true;
	}

	/**
	 * Print the jokes and current quote on the main board.
	 * @param {array}	jokes			A joke array given by the server.
	 * @param {string}	quote			A quote array given by the server.
	 */
	printJokes(jokes, quote)
	{
		this.emptyBackground();
		this.printQuote(quote);

		// We go through the whole array and for each element we create a card.
		jokes.forEach( element => 
		{
			let html = 
			`<div class="card cardPlayer" style="width: 18rem;">
				<div class="card-header">` + element.id + `</div>
				<div class="card-body"><p class="card-text">` + element.joke + `</p></div>
			</div>`
			this.cards.innerHTML += html;
		});

		// When we printed we return true.
		return true;
	}
}