/**
 * Representation of our Control socket class. This class emits socket from our server.
 * @class
 */
class ControlsSocket
{
	/**
	 * Create a controls socket instance with multiples emit and a sound to launch when the player played.
	 */
	constructor ()
	{
		// We call it stockSocket to remove the ambiguity and to use it as a generic socket.
		this.stockSocket = io();

		this.cardAudio = new Howl({ src: ["audio/card.mp3"] });

		// Waiting for the signal of connection to store the id of the stockSocket in the page.
		this.stockSocket.on('connect', () => 
		{
			document.querySelector("#playerID").innerHTML += this.stockSocket.id;
		});

		// Waiting for the signal resetGameUI to reset the player when the party is over.
		this.stockSocket.on('resetGameUI', () => window.location.reload());

		// Waiting for the signal resetGameUI to reset the player if he is still connected from a non existing game.
		this.stockSocket.on('oldPlayerReset', (playerID) => 
		{
			if (playerID == this.stockSocket.id)
				window.location.reload();
		});

		// Waiting for the signal selectCardClient to play a sound when the player voted or played a card.
		this.stockSocket.on("selectedCardClient", () =>
		{
			if (!this.cardAudio.playing())
				this.cardAudio.play();
		});

		// When the socket is created we send register.
		this.stockSocket.emit('register');
	}

	/**
	 * Emit the message play to our server with the card parameter to play a card.
	 * @param {JokeCard}	card	A card given by the clicked control ui.
	 */
	playCard(card)
	{
		this.stockSocket.emit('play', card);
	}

	/**
	 * Emit the message playedCard to our server with the card parameter to vote for a card.
	 * @param {JokeCard}	card	A card given by the clicked control ui.
	 */
	chosePlayedCard(card)
	{
		this.stockSocket.emit('playedCard', card);
	}
}