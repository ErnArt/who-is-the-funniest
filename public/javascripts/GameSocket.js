/**
 * Representation of our Game socket class. This class emits and waits for socket from our server.
 * @class
 */
class GameSocket
{
	/**
	 * Create a game socket instance with multiples listeners for our server emissions. 
	 * stopFlag is used to "loop" a sound because of the setInterval asking to launch it.
	 * @param {GameUI}	ui	The main game board.
	 */
	constructor(ui)
	{
		this.ui = ui;
		this.socket = io();
		this.stopFlag = false;

		this.audioSetup();
		this.socketListenersSetup();
	}

	/**
	 * Setup of our sockets and their listeners.
	 */
	socketListenersSetup() 
	{
		// Waiting for the signal to print a quote.
		this.socket.on('quote', (quote) => 
		{
			if (!this.backgroundAudio.playing())
				this.backgroundAudio.play();
			this.ui.printQuote(quote.joke);
		});

		// Waiting for the signal to print all played joke cards and when it's done emit a signal for our server.
		this.socket.on('allJokes', (jokes, quote) => 
		{
			if (!this.drumsStartAudio.playing() && !this.stopFlag)
				this.drumsStartAudio.play();
			if (this.ui.printJokes(jokes, quote.joke)) 
			{
				this.socket.emit('printCardsPlayedServer', jokes);
			}
		});

		// Waiting for the signal to print the leaderboard and when it's done emit a signal for our server after 5 seconds.
		this.socket.on('printLeaderboard', (leaderboard, winningCards, isFinished) =>
		{
			this.drumsMiddleAudio.stop();

			if (isFinished)
				if (!this.winningAudio.playing())
					this.winningAudio.play();

			if (this.ui.printLeaderboard(leaderboard, winningCards, isFinished))
				setTimeout(() => 
				{
					this.socket.emit('printedLeaderboard');
				}, 5 * 1000);
		});

		// Waiting for the signal to reset the board.
		this.socket.on('resetGameUI', () => this.ui.emptyBackground());
	}

	/**
	 * Setup of our audio objects and their listeners.
	 */
	audioSetup()
	{
		this.backgroundAudio = new Howl({ src: ["audio/background.mp3"] });
		this.drumsStartAudio = new Howl({ src: ["audio/drumsStart.mp3"] });
		this.drumsMiddleAudio = new Howl({ src: ["audio/drumsMiddle.mp3"], loop: true });
		this.drumsEndAudio = new Howl({ src: ["audio/drumsEnd.mp3"] });
		this.applauseAudio = new Howl({ src: ["audio/applause.mp3"] });
		this.winningAudio = new Howl({ src: ["audio/winning.mp3"] });

		this.audioListenersSetup();
	}

	audioListenersSetup()
	{
		// Waiting for the beginning of the first sound. We put it flag to true to avoid launching it multiple time.
		this.drumsStartAudio.on('play', () =>
		{
			this.backgroundAudio.pause();
			this.stopFlag = true;
		});

		// Waiting for the end of the first sound.
		this.drumsStartAudio.on('end', () =>
		{
			if (!this.drumsMiddleAudio.playing())
				this.drumsMiddleAudio.play();
		});

		// Waiting for the stop of the middle sound.
		this.drumsMiddleAudio.on('stop', () =>
		{
			if (!this.drumsEndAudio.playing())
				this.drumsEndAudio.play();
		});

		// Waiting for the end of the sound when stopped.
		this.drumsEndAudio.on('end', () =>
		{
			if (!this.applauseAudio.playing())
				this.applauseAudio.play();
			this.stopFlag = false;
		});
	}
}