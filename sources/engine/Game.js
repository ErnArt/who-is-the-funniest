'use strict'

const JokeCard = require('./JokeCard');
const Player = require('./Player');

// Import the two JSONs.
let jsonJokes = require("../../public/jsons/jokeCard.json");
let jsonQuotes = require("../../public/jsons/quotes.json");

/**
 * Give a random number between 0 and the max.
 * @param	{number} max	The maximum number we want to use.
 * @returns	{number}		A random number between 0 and the max.
 */
function getRandomInt(max) 
{
	return Math.floor(Math.random() * Math.floor(max));
}

/**
 * Compare two element of an array by the score property of an element of an array.
 * @param 	{number}	a 			An element of an array.
 * @param	{number}	b 			An element of an array.
 * @returns	{number}	comparison	0 if the elements are equel, 1 if b's score is lower than the a's score and -1 if not.
 */
function compareScore(a, b)
{
	let comparison = 0;
	if (a.score < b.score)
		comparison = 1;
	else if (a.score > b.score)
		comparison = -1;
	return comparison;
}

/**
 * Representation of our main Game class.
 * @class
 */
class Game
{
	/**
	 * Create a game instance. Calls resetGame() to be able to easily reset the game in our server.
	 */
	constructor()
	{
		this.resetGame();
	}

	/**
	 * Reset the game with new players and cards. Called in the constructor. 
	 * It has an object of the different players, of the leaderboard, of it quote cards and jokes cards.
	 * It then parse the various jokes and quotes from a JSON and take a quote.
	 */
	resetGame()
	{
		this.players = {};
		this.leaderboard = {};
		this.stockCardQuotes = {};
		this.stockCardJokes = {};
		this.fillQuotes();
		this.fillJokes();
		this.currentQuote = this.giveQuote();
	}

	/**
	 * Add a player to the players object if there are less than 4 players and still enough card to create a player.
	 * @param {number}	id	The id given to a player. In this code it's taken from the socket.
	 */
	register(id)
	{
		// Because our game let anyone connects and disconnects we limit the number of players at 4.
		if (Object.values(this.players).length < 4 && Object.values(this.stockCardJokes).length > 3)
		{
			// We then add the newly created Player and add it to the leaderboard.
			this.players[id] = new Player(id, this);
			this.leaderboard[id] = this.players[id];
			return true;
		}
		else
		{
			// We could make two distinct messages for more precision. Here it's just a console.log.
			console.log('Already too many players or not many cards !!!');
			return false;
		}
	}

	/**
	 * Give back cards of a player to the main stock joke cards.
	 * @param {number}	id	The id given to a player. In this code it's taken from the socket.
	 */
	giveBackCards(id)
	{
		Object.values(this.players[id].stockCardPlayer).forEach (element => 
		{
			this.stockCardJokes[element.id] = new JokeCard(element.id, element.joke);
		});
	}

	/**
	 * Remove a player from the players object and the leaderboard, only if he exists.
	 * We give back his current cards if he leaves the game before its end.
	 * @param {number}	id	The id given to a player. In this code it's taken from the socket.
	 */
	delist(id)
	{
		if (this.players[id] != undefined)
		{
			this.giveBackCards(id);
			delete this.players[id];
			delete this.leaderboard[id];
		}
	}

	/**
	 * Fill the stock of quote cards from parsing the JSON.
	 */
	fillQuotes()
	{
		jsonQuotes.forEach(element => 
		{
			this.stockCardQuotes[element.id] = new JokeCard(element.id, element.joke);
		});
	}

	/**
	 * Fill the stock of joke cards from parsing the JSON.
	 */
	fillJokes()
	{
		// Open JsonJokes
		jsonJokes.forEach(element => 
		{
			this.stockCardJokes[element.id] = new JokeCard(element.id, element.joke);
		});
	}

	/**
	 * Pick a random element from an object by having a random number with getRandomInt and going through one element at a time and
	 * stop when arriving to the random number. Because it's not an array we can't just take a random element with length
	 * because we could have deleted cards (ex: if we picked 25 and pick again 25 will be undefined).
	 * @param {object}	object	A generic object. Can be the stockCardQuotes or stockCardJokes.
	 */
	pickRandomElementObject(object)
	{
		let count = 0;
		let limitNumber = getRandomInt(Object.values(object).length);
		for (let element in object)
		{
			if (count == limitNumber)
				return element;
			count++;
		}
	}

	/**
	 * Draw a quote card from the stock and remove it from the stock befor returning it.
	 * @returns	{JokeCard}	A quote.
	 */
	giveQuote()
	{
		let cardNumber = this.pickRandomElementObject(this.stockCardQuotes);
		let quote = this.stockCardQuotes[cardNumber];
		delete this.stockCardQuotes[cardNumber];
		return quote;
	}

	/**
	 * Take the object of played card from the server and sort it to only keep the cards with the highest score.
	 * @param	{object}	nonSortedObjectArray	An object of all the played card from the server.
	 * @returns	{array}		winnersArray			A sorted array with the highest score card. There can be multiple cards.
	 */
	keepWinners(nonSortedObjectArray)
	{
		// Sort nonSortedObjectArray with the custom compareScore function from above.
		let sortedArray = Object.values(nonSortedObjectArray).sort(compareScore);

		// Create a array of cards with the highest score card, the first element of sortedArray.
		let winnersArray = [sortedArray[0]];

		// Go through all the sortedArray to verify if there are other cards with the same highest score.
		// If so they are pushed to the winnerArray.
		for (let i = 1; i<sortedArray.length; i++)
			if (sortedArray[0].score == sortedArray[i].score)
				winnersArray.push(sortedArray[i])

		return winnersArray;
	}

	/**
	 * Update the score to the leaderboard from the winners array.
	 * @param {array}	winners		A sorted array with all the winners of the current turn.
	 */
	updateScore(winners)
	{
		// Go through the whole winners array to increase the score of the winning player and updating the leaderboard.
		for (let i = 0; i < winners.length; i++)
		{
			this.players[winners[i].player].score += 1;
			this.leaderboard[winners[i].player].score = this.players[winners[i].player].score;
		}
	}

	/**
	 * Return a copy of the leaderboard as a sorted array.
	 * @returns {array}	A sorted array of the leaderboard.
	 */
	getSortedLeaderboard()
	{
		return Object.values(this.leaderboard).sort(compareScore);
	}

	/**
	 * Determine if the game is finished (if we don't have any more joke and quote cards to draw or if a player has the highest score).
	 * @returns	{bool}	True if the game is finished, False if not.
	 */
	isFinished()
	{
		if (Object.values(this.stockCardJokes).length < 4 || Object.values(this.stockCardQuotes).length == 0)
			return true;

		for (let element in this.players)
			if (this.players[element].score >= 3)
				return true;

		return false;
	}
}

module.exports = Game;