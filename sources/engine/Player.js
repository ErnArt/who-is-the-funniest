'use strict'

/**
 * Representation of our Player class.
 * @class
 */
class Player
{
	/**
	 * Create a player instance. It has an object of the jokes cards, a score and an id.
	 * @param {number}	id		The id given to a player. In this code it's taken from the socket.
	 * @param {Game}	game	A Game instance.
	 */
	constructor(id, game)
	{
		this.id = id;
		this.score = 0;
		this.stockCardPlayer = {};
		
		// Draw 4 cards from game.
		for(let i = 0; i < 4 ; i++)
			this.drawCard(game);

		// Add the player to the leaderboard.
		game.leaderboard[id] = this;
	}

	/**
	 * Play a specific card by copying it before deleting it and return it.
	 * @param 	{number}	id			The id given to a joke card. In this code it's taken from the server.
	 * @returns	{JokeCard}	copyCard	The card played to pick it in our server.
	 */
	playCard(id)
	{
		let copyCard = this.stockCardPlayer[id];
		delete this.stockCardPlayer[id];
		return copyCard;
	}

	/**
	 * Draw a card from the game stock jokes cards. We pick a random card and add it to the player stock. 
	 * We then add the player id to the card and delete the card from the stock of game.
	 * @param 	{Game}		game	A Game instance.
	 */
	drawCard(game)
	{
		if (Object.values(this.stockCardPlayer).length < 4)
		{
			let cardNumber = game.pickRandomElementObject(game.stockCardJokes);
			this.stockCardPlayer[cardNumber] = game.stockCardJokes[cardNumber];
			this.stockCardPlayer[cardNumber].player = this.id;
			delete game.stockCardJokes[cardNumber];
		}
	}
}

module.exports = Player;