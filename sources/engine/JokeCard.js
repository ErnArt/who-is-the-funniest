'use strict'

/**
 * Representation of our JokeCard class.
 * @class
 */
class JokeCard
{
	/**
	 * Create a joke card instance. It has an id, a player, a joke and a score for storing the "highest score" when voted.
	 * Here the player attribute is 0 until the card is picked by a player.
	 * @param {number} id		The id given to a joke card. In this code it's taken from the JSON.
	 * @param {string} text 	The joke text given to a joke card. In this code it's taken from the JSON.
	 */
	constructor(id, text)
	{
		this.id = id;
		this.player = 0;
		this.joke = text;
		this.score = 0;
	}
}

module.exports = JokeCard;