const Game = require('../engine/Game');
const game = new Game();

const socketio = require('socket.io');

/**
 * The main function that emits and waits for socket of our clients. It has three main variables :
 * cardPlayedArray as an object of all played card, count as a counter of action and stopFlag as a boolean flag.
 * @param {server}	server	A http server.
 */
function io(server)
{
	let cardPlayedArray = {};
	let count = 1;
	let countEndParty = 1;
	let stopFlag = true;

	/**
	 * Input output of the server.
	 */
	const io = socketio(server);
	io.on('connection', (socket) =>
	{
		// Waiting for the signal register to register a player.
		socket.on('register', () => 
		{
			// Only if the player was registered we give his cards.
			if (game.register(socket.id))
			{
				let cardClient = (Object.values(game.players[socket.id].stockCardPlayer));
				io.emit('printCardsClient', socket.id, cardClient);
			}
			else
				io.emit("tooManyPlayers", socket.id);
		});
		
		// Waiting for the signal play to play a card.
		socket.on('play', (card) =>
		{
			// To avoid launching multiple time playCard and avoid playing if the game is finished.
			// We first verifies if the player still exists in the game.
			// We emit a signal to start a sound on the client to tell him he played.
			if (
					game.players[socket.id] != undefined &&
						(game.players[card.player].stockCardPlayer[card.id] != undefined && 
						cardPlayedArray[card.player] == undefined && 
						!game.isFinished())
				)
				{
					cardPlayedArray[card.player] = game.players[card.player].playCard(card.id);
					io.emit("selectedCardClient");
				}

			// If a player from an older game tries to play we send him a reset message. 
			else if (game.players[socket.id] == undefined)
				io.emit('oldPlayerReset', socket.id);
		});

		// Waiting for the signal playedCard to count each player's vote and updating the score.
		// We emit a signal to start a sound on the client to tell him he voted.
		socket.on('playedCard', (card) => 
		{
			io.emit("selectedCardClient");

			// We increase the score of the voted card.
			cardPlayedArray[card.player].score += 1;

			// Wait for every players using the length of game.players to update the score, put the flag to false and emit a signal.
			if (count == Object.values(game.players).length)
			{
				let winningCards = game.keepWinners(cardPlayedArray);
				game.updateScore(winningCards);

				stopFlag = false;
				io.emit('printLeaderboard', game.getSortedLeaderboard(), Object.values(winningCards), game.isFinished());
			}

			// At each loop we increase the counter.
			count += 1;
		});

		// Waiting for the signal printCardsPlayedServer to emit printCardsPlayed with the jokes cards played.
		socket.on('printCardsPlayedServer', (jokes) => io.emit('printCardsPlayed', jokes));

		// Waiting for the signal printedLeaderboard as an end of turn to give to each players a card and reset the three main variables.
		socket.on('printedLeaderboard', () => 
		{
			Object.values(game.players).forEach( element => 
			{
				element.drawCard(game);
			});

			game.currentQuote = game.giveQuote();
			count = 1;
			cardPlayedArray = {};
			stopFlag = true;
		});

		// Waiting for the signal disconnect to remove a player from the main game. If there are no more players we reset the game.
		socket.on('disconnect', () => 
		{
			if (Object.values(game.players) == 0)
			{
				io.emit("resetGameUI");
				game.resetGame();
			}
			game.delist(socket.id);
		});
	});


	/**
	 * Loop game launched once a second.
	 */
	setInterval(() =>
	{
		// While the game is not finished.
		if (!game.isFinished())
		{
			// Condition when all players have played and avoid launching the game without any players.
			if (Object.values(cardPlayedArray).length == Object.values(game.players).length && Object.values(game.players).length > 0)
			{
				// When they have all played we give to each players the played cards for them to vote.
				if (stopFlag)
					io.emit('allJokes', Object.values(cardPlayedArray), game.currentQuote);
			}

			// Else we print the currentQuote and send all the cards for each players with their is as an argument.
			else
			{
				io.emit('quote', game.currentQuote);
				Object.values(game.players).forEach( element => 
				{
					io.emit('printCardsClient', element.id, Object.values(element.stockCardPlayer));
				});
			}
		}

		// When the game is finished.
		else
		{
			console.log("End of game");

			// We wait 5 seconds before resetting the game
			if (countEndParty > 5)
			{
				io.emit("resetGameUI");
				game.resetGame();
				countEndParty = 1;
			}

			countEndParty += 1;
		}
	}, 1000 / 1);
}

module.exports = io;