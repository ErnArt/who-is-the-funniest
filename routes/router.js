var express = require('express');
var router = express.Router();

const indexController = require('../controllers/indexController');
const gameController = require('../controllers/gameController');
const controlsController = require('../controllers/controlsController');

router.get('/', indexController.index);
router.get('/game', gameController.game);
router.get('/controls', controlsController.controls);

module.exports = router;