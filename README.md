# Who is The Funniest

## Description
This project is a NodeJS / Express test project for our third year of Undergraduate in IT Studies. It's a simple card game based on Cards Against Humanity, using the socket.io library to let people choose their favorites jokes. This project uses the CI/CD functionnalities of Gitlab to update the app when updating the master branch of the code to our main Heroku application. Here is a link for accessing the various applications, documentation and repository. https://ernart.gitlab.io/who-is-the-funniest/

This project is made of :
- the framework (NodeJS) for our main application.
- the framework (Express) for routing our application.
- the framework (EJS), Embedded JavaScript templates for better modularity.
- the daemon (nodemon) for refreshing the application when a change is made.
- the API (JSDoc) for documenting our app.
- the theme (better-docs) for a neater theme.
- the Javascript library (Howler) for more functionalities for audio objects.

## Dependencies
- npm

## Libraries installations
```sh
npm install
```

## Usage
```sh
npm run start

# Open your favorite browser
vivaldi localhost:3000
chrome localhost:3000
firefox localhost:3000
```
You can access the main version here : https://who-is-the-funniest.herokuapp.com.

For the development version : https://who-is-the-funniest-dev.herokuapp.com

## Documentation
```sh
jsdoc sources/engine sources/io public/javascripts/ -t ./node_modules/better-docs --readme README.md

# Open your favorite browser
vivaldi out/index.html
chrome out/index.html
firefox out/index.html
```
You can access the documentation here : https://ernart.gitlab.io/who-is-the-funniest/doc/index.html.

## Rules
To play to Who is The Funniest, we recommend being at least 3 players to avoid voting for each other.
A sentence to complete will be displayed at each round in the Game page, each player have 4 cards with word or expression from which he will have to choose to make the funniest sentence in the Controls page. They vote for cards from others players to elect the funniest. A point is added to the player's score. The game ends when a player has 3 points. When they play a card or vote for one a sound is made through all players to notify them that someone played. When the game is over there is neat jingle with a text message before restarting.

## Organization
We work as a team of three :
- Ernesto Artigas, maintainer of the main project, Gitmaster, Sound and Graphic Designer.
- Audrey Dewaele, Back-End developer, focused on the socket.io framework.
- Alexandre Fovet, Back-End developer, focused on the Game Engine.

From the main master branch we create another branch used for development. Audrey and Alexandre work on two distinct branches from development to have total control on their work. We then test with Unit Tests created by Alexandre when starting the project to be sure we are all in the same path. When the Unit Tests are passed the developers ask to the gitmaster to verify the functionnalities and to test them on his machine, then they make a merge request to the development branch. It is then pushed to our test Heroku application to test it further. If it passed, we close the issues and we complete the milestone before merging the branch to the master one. All Milestones and Issues with the planning are posted on our Gitlab project page.We have used Visual Studio Live Share which is an extension of Visual Studio Code which allows to code together in the same folder and to share the terminal. We have a discord server too where we are connected everyday in audio. 